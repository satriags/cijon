<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $title; ?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</head>

<body class="container">

    <h2>Isi Data Karyawan JSON</h2>

    <form id="form" method="post" enctype="multipart/form-data">

        <div class="mb-3">
            <label class="form-label" for="fname">Nama Lengkap</label>
            <input class="form-control" style="width:200px;" type="text" id="nama" name="nama">
        </div>
        <div class="mb-3">
            <label class="form-label" for="lname">Tanggal Lahir</label>
            <input class="form-control" style="width:200px;" type="date" id="tanggallahir" name="tanggallahir">

        </div>
        <div class="mb-3">
            <label class="form-label" for="lname">Foto</label>
            <input class="form-control" style="width:200px;" type="file" accept="image/png" id="foto" name="foto">
        </div>

        <button class="btn btn-primary">Simpan</button>
        <!-- <input id="simpan" class="btn btn-primary rounded-pill w-25" value="Simpan"> -->
    </form>



    <table id="datalist" class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Tanggal Lahir</th>
                <th scope="col">Foto</th>
                <th colspan="2" width="5%">Opsi</th>
            </tr>
        </thead>
        <tbody id="loaddatatable">
        </tbody>
    </table>



    <script>
        $(document).ready(function() {


            refreshTable();

            function refreshTable() {
                let table = "";

                let result = getJSON('<?= base_url(); ?>/list', {});
                for (let i = 0; i < result.length; i++) {
                    table += "<tr>";
                    table += "<td>" + (parseInt(i) + 1) + "</td>";
                    table += "<td>" + result[i]['nama'] + "</td>";
                    table += "<td>" + result[i]['tanggallahir'] + "</td>";
                    table += "<td> <img src='<?= base_url() ?>/foto/" + result[i]['foto'] + "' width='100'/></td>";
                    table += "<td><a href='javascript:void(0)' class='rounded-pill btn btn-secondary btn-sm opsi'  data-id='" + result[i]['id'] + "' data-mode='edit'>Edit</a></td>";
                    table += "<td><a href='javascript:void(0)' class='rounded-pill btn btn-danger btn-sm opsi' data-id='" + result[i]['id'] + "' data-mode='hapus'>Hapus</a></td>";

                    table += "</tr>";
                }
                document.getElementById("loaddatatable").innerHTML = table;

            }

            $("a.opsi").click(function() {
                var id = $(this).data('id');
                var mode = $(this).data('mode');

                // JSON STRINGFY DI BERIKAN KEPADA BEBRAPA KASUS JIKA PARAMETER TIDAK MENJADI OBJECT SEUTUHNYA CEK PADA PAYLOAD 
                Swal.fire({
                    title: 'Do you want to save the changes?',
               
                    showCancelButton: true,
                    confirmButtonText: 'Save',
                    denyButtonText: `Don't save`,
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        Swal.fire('Saved!', '', 'success')
                    } else if (result.isDenied) {
                        Swal.fire('Changes are not saved', '', 'info')
                    }
                })
                let result = getJSON('<?= base_url(); ?>/opsi', JSON.stringify({
                    id: id,
                    mode: mode

                }));

                if (mode == "hapus") {
                    if (result.status == 'success') {
                        alertSweet(result.status, 'berhasil menghapus data', 'success');
                        refreshTable();
                    } else {
                        alertSweet(result.status, 'gagal menghapus data', 'success');
                        refreshTable();
                    }
                }


            });


            $("form#form").submit(function(e) {
                e.preventDefault();
                var fd = new FormData(this);

                let result = getJSON('<?= base_url(); ?>/create', fd);

                if (result.status == "success") {
                    alertSweet(result.status, 'berhasil menyimpan data', 'success');
                    refreshTable();
                    clearForm('#form');
                } else {
                    alertSweet(result.status, 'gagal menyimpan karena data sama', 'error');
                    refreshTable();
                    clearForm('#form');
                }
            });


            function clearForm(id) {
                $(id).closest('form').find("input[type=text],input[type=file], textarea, input[type=password], input[type=date]").val("");
            }


            function alertSweet(msg, desc, type) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'bottom-end',
                    showConfirmButton: false,
                    timer: 4000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                });
                return Toast.fire({
                    icon: type,
                    title: desc
                });
            }

            function getJSON(url, data) {
                return JSON.parse($.ajax({
                    type: 'POST',
                    url: url,
                    data: data,
                    dataType: 'json',
                    global: false,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(msg) {}
                }).responseText);
            }

        });
    </script>


</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

<script src="https://sumbrejeki.com/swalert/dist/sweetalert2.all.min.js"></script>

</html>