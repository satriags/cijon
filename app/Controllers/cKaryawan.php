<?php

namespace App\Controllers;

class cKaryawan extends BaseController
{

    public function index()
    {
        $data = [
            'title' => 'Input Data Karyawan'
        ];

        return view('vKaryawan', $data);
    }

    public function list()
    {

        $karyawanlist = $this->karyawan->get()->getResultArray();

        $data = array();
        foreach ($karyawanlist as $k) {
            array_push($data, $k);
        }
        return json_encode($data);
    }


    public function create()
    {
        $nama = $this->request->getVar('nama');
        $tanggallahir = $this->request->getVar('tanggallahir');
        $foto =   time() . "_" . $_FILES['foto']['name'];
        $foto_tmp = $_FILES['foto']['tmp_name'];


        $location =   "foto/" . $foto;

        $result = $this->karyawan->save([
            'nama' => $nama,
            'tanggallahir' => $tanggallahir,
            'foto' => $foto,
        ]);

        if ($result == true && move_uploaded_file($foto_tmp, $location)) {
            $status['status'] = 'success';
        } else {
            $status['status'] = 'failed';
        }
        return json_encode($status);
    }



    public function opsi()
    {
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body);


        if ($data->mode == "hapus") {

            $karyawanlist = $this->karyawan->where('id', $data->id)->first();
            @unlink('foto/' . $karyawanlist['foto']);

            $this->karyawan->delete($data->id);


            $status['status'] = 'success';
        } else {
        }


        return json_encode($status);
    }
}
